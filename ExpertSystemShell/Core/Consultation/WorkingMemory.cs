﻿using ExpertSystemShell.Core.KnowledgeStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Core.Consultation
{
    public class WorkingMemory
    {
        private List<Fact> _memory;

        public Variable CurrentVariable { get; set; }
        public List<Fact> CurrentMemory
        {
            get
            {
                if (_memory == null)
                    _memory = new List<Fact>();
                return _memory;
            }
            private set
            {
                _memory = value;
            }
        }
    }
}
