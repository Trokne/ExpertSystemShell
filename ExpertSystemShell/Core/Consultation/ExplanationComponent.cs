﻿using ExpertSystemShell.Core.KnowledgeStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Core.Consultation
{
    public class ExplanationComponent
    {
        private Stack<Rule> _rules;
        private Stack<Variable> _goals;

        public ExplanationComponent() { }

        public ExplanationComponent(ExplanationComponent expComponent)
        {
            Goals = new Stack<Variable>(expComponent.Goals.Select(x => new Variable(x)));
            Rules = new Stack<Rule>(expComponent.Rules.Select(x => new Rule(x)));
        }

        public Stack<Variable> Goals
        {
            get
            {
                if (_goals == null)
                    _goals = new Stack<Variable>();
                return _goals;
            }
            private set
            {
                _goals = value;
            }
        }
        public Stack<Rule> Rules
        {
            get
            {
                if (_rules == null)
                    _rules = new Stack<Rule>();
                return _rules;
            }
            private set
            {
                _rules = value;
            }
        }

        public string GetExplanation()
        {
            string result = "Объяснение: ";
            bool isBadResult = Rules.Count == 0;
            var rules = new Stack<Rule>(Rules);
            var goals = new Stack<Variable>(Goals);
            while (rules.Count > 0)
            {
                var rule = rules.Pop();
                rule.IsExplanation = true;
                var goal = goals.Pop();
                result += $"\n\nСработало правило: {rule.Name}\n{rule.Description}";
                result += $"\nДоказывалась цель: {goal.Name}";
                rule.IsExplanation = false;
            }
            if (isBadResult)
                result += "\nНи одно из правил не сработало";
            return result;
        }
    }

}
