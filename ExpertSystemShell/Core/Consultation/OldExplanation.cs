﻿using ExpertSystemShell.Core.KnowledgeStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Core.Consultation
{
    public static class OldExplanation
    {
        public static Variable MainGoal { get; set; }
        public static ExplanationComponent Explanation { get; set; }
        public static List<Fact> WorkingMemory { get; set; }

        public static void SetCopy(Variable mainGoal, ExplanationComponent expComponent, List<Fact> workingMemory)
        {
            MainGoal = new Variable(mainGoal);
            Explanation = new ExplanationComponent(expComponent);
            WorkingMemory = workingMemory.Select(x => new Fact(x)).ToList();
        }

        public static void Remove()
        {
            MainGoal = null;
            Explanation = null;
            WorkingMemory = null;
        }
    }
}
