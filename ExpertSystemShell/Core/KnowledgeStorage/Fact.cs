﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Core.KnowledgeStorage
{
    public class Fact
    {
        public enum FactType { Premise, Conclusion }
        
        public FactType Type { get; set; }
        public Variable Variable { get; set; }
        public DomainValue AcceptedValue { get; set; }

        public string Description
        {
            get
            {
                string splitter = Type == FactType.Premise ? "==" : ":=";
                return $"{Variable.Name} {splitter} {AcceptedValue.Value}";
            }
        }

        public Fact(Variable variable, DomainValue value)
        {
            Variable = variable;
            AcceptedValue = value;
        }

        public Fact()
        {
            Variable = new Variable();
            AcceptedValue = new DomainValue();
        }

        public Fact(Fact fact)
        {
            Type = fact.Type;
            Variable = new Variable(fact.Variable);
            AcceptedValue = new DomainValue(fact.AcceptedValue);
        }

        public bool IsContainVariable(Variable variable)
        {
            if (Variable == variable || 
                Variable.Name == variable.Name)
                return true;
            else return false;
        }

        public bool IsEqual(Fact fact)
        {
            return fact.Variable.Name == Variable.Name &&
                fact.AcceptedValue.Value == AcceptedValue.Value;
        }
    }
}
