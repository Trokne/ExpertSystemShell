﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Core.KnowledgeStorage
{
    public class Domain
    {
        private ObservableCollection<DomainValue> _values;

        public Domain(string name, params DomainValue[] values)
        {
            Name = name;
            _values = new ObservableCollection<DomainValue>(values);
        }

        public Domain (string name, List<DomainValue> values)
        {
            Name = name;
            _values = new ObservableCollection<DomainValue>(values);
        }

        public Domain()
        {
            Name = String.Empty;
            _values = new ObservableCollection<DomainValue>();
        }

        public Domain(Domain sourceDomain)
        {
            _values = new ObservableCollection<DomainValue>(sourceDomain.Values);
            Name = sourceDomain.Name;
        }

        public void CopyFromDomain(Domain newDomain)
        {
            Name = newDomain.Name;
            var values = new List<string>();
            foreach (var val in newDomain.Values)
                values.Add(val.Value);
            for (int i = 0; i < Values.Count; i++)
                Values[i].Value = values[i];
        }

        public string Name { get; set; }

        public ObservableCollection<DomainValue> Values
        {
            get
            {
                if (_values == null)
                {
                    _values = new ObservableCollection<DomainValue>();
                    return _values;
                }
                else return _values;
            }
            set
            {
                _values = value;
            }
        }

        public List<Variable> GetVariable()
        {
            var variables = ProductionShell.Data.Variables.Where(v => v.Domain == this);
            return variables.ToList();
        }

        public static bool IsEquals(Domain domainFirst, Domain domainSecond, bool isCheckName = true)
        {
            if (isCheckName && (domainFirst.Name != domainSecond.Name ||
                domainFirst.Values.Count != domainSecond.Values.Count)) return false;
            bool isEqualValues = true;
            for (int i = 0; i < domainFirst.Values.Count; i++)
            {
                if (domainFirst.Values[i].Value != domainSecond.Values[i].Value)
                    isEqualValues = false;
            }
            return isEqualValues;
        }

        public override string ToString() => Name;
    }
}
