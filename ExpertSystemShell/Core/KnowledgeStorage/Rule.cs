﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ExpertSystemShell.Core.KnowledgeStorage
{
    public class Rule
    {
        public string Name { get; set; }
        public ObservableCollection<Fact> Premises { get; set; }
        public ObservableCollection<Fact> Conclusions { get; set; }
        public bool IsExplanation { get; set; }
        [XmlIgnore]
        public int Level { get; set; }


        public string Description
        {
            get
            {
                string result = !IsExplanation ? "ЕСЛИ " : "Т.К. ";
                result += GetFacts(Premises, " == ", false);
                result += Environment.NewLine + "     ТО " + GetFacts(Conclusions, " := ", true);
                return result;
            }
        }

        public Rule(string name, ObservableCollection<Fact> premises, ObservableCollection<Fact> conclusions)
        {
            Name = name;
            Premises = new ObservableCollection<Fact>(premises);
            Conclusions = new ObservableCollection<Fact>(conclusions);
        }

        public Rule()
        {
            Premises = new ObservableCollection<Fact>();
            Conclusions = new ObservableCollection<Fact>();
            Name = string.Empty;
        }

        public Rule(Rule rule)
        {
            Premises = new ObservableCollection<Fact>(rule.Premises.Select(x => new Fact(x)));
            Conclusions = new ObservableCollection<Fact>(rule.Conclusions.Select(x => new Fact(x)));
            Name = rule.Name;
        }

        private string GetFacts(IList<Fact> facts, string splitter, bool IsEnd)
        {
            string result = "";
            foreach (var fact in facts)
            {
                result += fact.Variable.Name;
                result += $" {splitter} ";
                result += fact.AcceptedValue.Value;
                result += IsEnd ? ", " : " И ";
            }
            if (!IsEnd && result.Length > 0)
                result = result.Remove(result.Length - 3);
            if (IsEnd && result.Length > 0)
                result = result.Remove(result.Length - 2);
            if (String.IsNullOrWhiteSpace(result))
                result = "<ПУСТО>";
            return result;
        }

        public bool IsContain(Fact fact)
        {
            foreach (var prem in Premises)
            {
                if (prem.IsEqual(fact))
                    return true;
            }
            foreach (var conclusion in Conclusions)
            {
                if (conclusion.IsEqual(fact))
                    return true;
            }
            return false;
        }

        public override string ToString() => Name;

    }
}