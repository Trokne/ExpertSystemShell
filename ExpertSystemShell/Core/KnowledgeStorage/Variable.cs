﻿using ExpertSystemShell.Core.Handlers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ExpertSystemShell.Core.KnowledgeStorage
{
    public class Variable
    {
        public enum VariableType
        {
            [Description("Выводимая")]
            Deducible,
            [Description("Запрашиваемая")]
            Requested,
            [Description("Выводимо-запрашиваемая")]
            DeducibleRequested
        }

        [XmlIgnore]
        public string RuleName { get; set; }

        public string Name { get; set; }
        public string Question { get; set; }
        public Domain Domain { get; set; }
        public VariableType Type { get; set; }
        public string TypeToDescription => Type.ToDescription();

        public Variable()
        {
            Type = VariableType.Deducible;
            Question = String.Empty;
            Domain = new Domain();
            Name = String.Empty;
        }

        public Variable(Variable variable)
        {
            CopyFromVariable(variable);
        }

        public List<Rule> FindInRules()
        {
            List<Rule> rules = new List<Rule>();
            foreach (var rule in ProductionShell.Data.Rules)
            {
                var conclusions = rule.Conclusions;
                var premises = rule.Premises;
                if (conclusions.Any(x => x.IsContainVariable(this)) ||
                    premises.Any(x => x.IsContainVariable(this)))
                    rules.Add(rule);
            }
            return rules;
        }

        public void CopyFromVariable(Variable variable)
        {
            Name = variable.Name;
            Question = variable.Question;
            Domain = variable.Domain;
            Type = variable.Type;
        }

        public override string ToString() => Name;

    }
}
