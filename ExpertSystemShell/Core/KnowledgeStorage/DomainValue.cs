﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Core.KnowledgeStorage
{
    public class DomainValue
    {
        public string Value { get; set; }

        public DomainValue() => Value = String.Empty;
        public DomainValue(string val) => Value = val;
        public DomainValue(DomainValue val) => Value = val.Value;
    }
}
