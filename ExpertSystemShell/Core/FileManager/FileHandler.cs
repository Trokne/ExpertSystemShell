﻿using ExpertSystemShell.Core.Consultation;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace ExpertSystemShell.Core.FileManager
{
    public static class FileHandler
    {
        public static string ProjectPath { get; set; }
        private static string _filter = "База знаний (*.XML)|*.xml" + "|Все файлы (*.*)|*.*";

        public static string GetTitle()
        {
            string title = "";
            if (String.IsNullOrEmpty(ProjectPath))
                title += "Безымянная.xml";
            else title += Path.GetFileName(ProjectPath);
            return title + " - базa знаний";
        }

        public static void SaveAs()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = _filter
            };
            if (saveFileDialog.ShowDialog() == true)
            {
                ProjectPath = saveFileDialog.FileName;
                ProductionShell.Save(ProjectPath);
            }
        }

        public static void Save()
        {
            if (String.IsNullOrEmpty(ProjectPath))
                SaveAs();
            else ProductionShell.Save(ProjectPath);
        }

        public static void Open()
        {
            var openSaveDialog = new OpenFileDialog
            {
                Filter = _filter,
                CheckFileExists = true
            };
            if (openSaveDialog.ShowDialog() == true)
            {
                var projectPath = openSaveDialog.FileName;
                if (!ProductionShell.Load(projectPath))
                    return;
                ProjectPath = projectPath;
                OldExplanation.Remove();
            }
        }

        public static void CheckSave()
        {
            if (ProductionShell.Data.Domains.Count == 0)
                return;
            var dlgResult = MessageBox.Show("Вы хотите сохранить свою работу? " +
                "Возможно, база знаний содержит некоторые изменения",
                "Создание новой БЗ",
                MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (dlgResult == MessageBoxResult.Yes)
                Save();
        }
    }
}
