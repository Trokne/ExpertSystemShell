﻿using ExpertSystemShell.Core.FileManager;
using ExpertSystemShell.Core.Handlers;
using ExpertSystemShell.Core.KnowledgeStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ExpertSystemShell.Core
{
    [Serializable]
    public class ProductionShell
    {
        private static ProductionShell _instance;
        public static ProductionShell Data
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ProductionShell();
                }
                return _instance;

            }
            private set
            {
                _instance = value;
            }
        }

        public ProductionShell(ProductionShell lastData)
        {
            Domains = new ObservableCollection<Domain>(lastData.Domains.Select(x => new Domain(x)));
            Variables = new ObservableCollection<Variable>(lastData.Variables.Select(x => new Variable(x)));
            Rules = new ObservableCollection<Rule>(lastData.Rules.Select(x => new Rule(x)));
            DomainNameIndex = VariableNameIndex = RuleNameIndex = 0;
        }

        public ProductionShell()
        {
            Domains = new ObservableCollection<Domain>();
            Variables = new ObservableCollection<Variable>();
            Rules = new ObservableCollection<Rule>();
            DomainNameIndex = VariableNameIndex = RuleNameIndex = 0;
        }

        public ObservableCollection<Domain> Domains { get; set; }
        public ObservableCollection<Variable> Variables { get; set; }
        public ObservableCollection<Rule> Rules { get; set; }
        public int DomainNameIndex { get; set; }
        public int VariableNameIndex { get; set; }
        public int RuleNameIndex { get; set; }

        public static void New()
        {
            Data = new ProductionShell();
            FileHandler.ProjectPath = null;
        }

        public static void Save(string fileName)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(ProductionShell));
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                formatter.Serialize(fs, Data);
            }
        }

        public static bool Load(string fileName)
        {
            try
            {
                var _tempInstance = new ProductionShell();
                XmlSerializer formatter = new XmlSerializer(typeof(ProductionShell));
                using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
                {
                    _tempInstance = (ProductionShell)formatter.Deserialize(fs);
                }
                Data = _tempInstance;
                ResetDomainsLinks();
                ResetVariableLinks();
                return true;
            }
            catch (Exception e)
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка загрузки БЗ", e.Message);
                return false;
            }
        }

        private static void ResetVariableLinks()
        {
            foreach (var rule in Data.Rules)
            {
                foreach (var prem in rule.Premises)
                    prem.Variable = Data.Variables.First(x => x.Name == prem.Variable.Name);
                foreach (var conc in rule.Conclusions)
                    conc.Variable = Data.Variables.First(x => x.Name == conc.Variable.Name);
            }
        }

        private static void ResetDomainsLinks()
        {
            foreach (var variable in Data.Variables)
            {
                var domain = Data.Domains.First(x => x.Name == variable.Domain.Name);
                variable.Domain = domain;
            }
        }

        public void RemoveDomain(Domain domain)
        {
            RemoveVariable(domain);
            bool isRemovedInDomains = Domains.Remove(domain);
            if (!isRemovedInDomains)
            {
                var remDomain = Domains.First(d => d.Name == domain.Name);
                if (remDomain != null) Domains.Remove(remDomain);
            }
        }

        private void RemoveVariable(Domain domain)
        {
            var variables = domain.GetVariable();
            for (int i = 0; i < variables.Count(); i++)
                RemoveVariable(variables[i]);
        }

        public void RemoveVariable(Variable variable)
        {
            var rules = variable.FindInRules();
            for (int i = 0; i < rules.Count(); i++)
                Data.Rules.Remove(rules[i]);
            Data.Variables.Remove(variable);
        }
    }
}
