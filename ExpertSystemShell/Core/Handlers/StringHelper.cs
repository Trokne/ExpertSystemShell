﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpertSystemShell.Core.Handlers
{
    public static class StringHelper
    {
        public static string GetShort(this string text)
        {
            if (text.Length < 12)
                return text;
            string result = text.Substring(0, 12);
            return result + "...";
        }

        public static string GetCopyName<T>(string sourceName, ObservableCollection<T> collection)
        {
            var copyNameIndex = -1;
            var name = "";
            do
            {
                name = sourceName + "_Copy" + ++copyNameIndex;
            }
            while (collection.Any(x => x.ToString() == name));
            return name;
        }
    }
}
