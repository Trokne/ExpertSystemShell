﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExpertSystemShell.Core.Handlers
{
    public static class MessageBoxHelper
    {
        public static void ShowErrorMessage(string title, string text)
            => MessageBox.Show(text, title, MessageBoxButton.OK, MessageBoxImage.Error);
    }
}
