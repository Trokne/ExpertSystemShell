﻿using ExpertSystemShell.Core.Consultation;
using ExpertSystemShell.Core.KnowledgeStorage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell.UI.Windows.Consultation
{
    /// <summary>
    /// Логика взаимодействия для ExplanationWindow.xaml
    /// </summary>
    public partial class ExplanationWindow : Window
    {
        private ExplanationComponent _explanation;
        private Variable _mainGoal;
        private ObservableCollection<Fact> _workingMemory;
        private bool isExpanded = true;

        public ExplanationWindow(ExplanationComponent explanation, Variable mainGoal, List<Fact> workingMemory)
        {
            InitializeComponent();
            _explanation = explanation;
            _mainGoal = mainGoal;
            _workingMemory = new ObservableCollection<Fact>(workingMemory);
            InitializeExplanation();
        }

        private void InitializeExplanation()
        {
            lvWorkingMemory.ItemsSource = _workingMemory;
            bool isBadResult = _explanation.Rules.Count == 0;
            var rules = new Stack<Rule>(_explanation.Rules.Reverse());
            var goals = new Stack<Variable>(_explanation.Goals.Reverse());

            TreeViewItem lastItem = null, firstItem = null;
            int lastLevel = -1;
            if (rules.Count > 0)
            {
                var rule = rules.Pop();
                var goal = goals.Pop();
                var ruleItem = new TreeViewItem
                {
                    Header = rule.Name + ": " + rule.Description
                };
                var goalItem = new TreeViewItem
                {
                    Header = "Цель: " + goal.Name
                };
                tvExplanation.Items.Add(ruleItem);
                tvExplanation.Items.Add(goalItem);
                lastItem = firstItem = goalItem;
            }
            else
            {
                var goalItem = new TreeViewItem
                {
                    Header = "Главная цель: " + _mainGoal.Name
                };
                tvExplanation.Items.Add(goalItem);
            }
            while (rules.Count > 0)
            {
                var rule = rules.Pop();
                var goal = goals.Pop();

                var ruleItem = new TreeViewItem
                {
                    Header = rule.Name + ": " + rule.Description
                };
                var goalItem = new TreeViewItem
                {
                    Header = "Цель: " + goal.Name
                };

                if (rule.Level == lastLevel)
                {
                    (lastItem.Parent as TreeViewItem).Items.Add(ruleItem);
                    (lastItem.Parent as TreeViewItem).Items.Add(goalItem);
                }
                else if (rule.Level > lastLevel)
                {
                    lastItem.Items.Add(ruleItem);
                    lastItem.Items.Add(goalItem);
                }
                else if (rule.Level < lastLevel)
                {
                    firstItem.Items.Add(ruleItem);
                    firstItem.Items.Add(goalItem);
                }
                lastLevel = rule.Level;
                lastItem = goalItem;
            }
        }

        void SetTreeViewItems(object obj, bool expand)
        {
            if (obj is TreeViewItem)
            {
                ((TreeViewItem)obj).IsExpanded = expand;
                foreach (object obj2 in ((TreeViewItem)obj).Items)
                    SetTreeViewItems(obj2, expand);
            }
            else if (obj is ItemsControl)
            {
                foreach (object obj2 in ((ItemsControl)obj).Items)
                {
                    if (obj2 != null)
                    {
                        SetTreeViewItems(((ItemsControl)obj).ItemContainerGenerator.ContainerFromItem(obj2), expand);

                        TreeViewItem item = obj2 as TreeViewItem;
                        if (item != null)
                            item.IsExpanded = expand;
                    }
                }
            }
        }

        private void expCollapseLink_Click(object sender, RoutedEventArgs e)
        {
            expCollapseText.Text = isExpanded ?
                "(скрыть все)" : "(раскрыть все)";
            SetTreeViewItems(tvExplanation, isExpanded);
            isExpanded = !isExpanded;
        }
    }
}
