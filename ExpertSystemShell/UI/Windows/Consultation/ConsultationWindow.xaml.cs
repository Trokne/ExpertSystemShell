﻿using ExpertSystemShell.Core;
using ExpertSystemShell.Core.Consultation;
using ExpertSystemShell.Core.KnowledgeStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ExpertSystemShell.UI.Windows.Consultation
{
    /// <summary>
    /// Логика взаимодействия для ConsultationWindow.xaml
    /// </summary>
    public partial class ConsultationWindow : Window
    {
        private WorkingMemory _workingMemory;
        private ExplanationComponent _explanationComponent;
        private Variable _mainGoal;
        private Thread _mainQuestionThread;

        private bool isSpecialQuestion;
        private bool _isAnswered;

        public ConsultationWindow()
        {
            InitializeComponent();
            NewConsultation();
        }

        private void AddMessage(Message msg)
        {
            spMessages.Children.Add(msg);
            svMessages.ScrollToEnd();
        }

        private void NewConsultation()
        {
            _workingMemory = new WorkingMemory();
            _explanationComponent = new ExplanationComponent();
            if (ProductionShell.Data.Variables.Count != 0)
                AddMessage(new Message(Message.MessageType.Question,
                "Какова цель консультации?"));
            else AddMessage(new Message(Message.MessageType.Question,
                "Ошибка начала консультации: список переменных пуст.", "#FFBACC"));
            cbAnswer.DisplayMemberPath = "Name";
            cbAnswer.ItemsSource = ProductionShell.Data.Variables;
            if (cbAnswer.Items.Count != 0)
                cbAnswer.SelectedIndex = 0;
            isSpecialQuestion = true;
        }

        private void btnSetAnswer_Click(object sender, RoutedEventArgs e)
        {
            if (isSpecialQuestion)
            {
                isSpecialQuestion = false;
                AddMessage(new Message(Message.MessageType.Answer, 
                    "Цель консультации: " + ((Variable)cbAnswer.SelectedItem).Name));
                _mainGoal = (Variable)cbAnswer.SelectedItem;
                _mainQuestionThread = new Thread(ProveMainGoal);
                _mainQuestionThread.Start();
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() =>
                {
                    var deducedFact = new Fact()
                    {
                        AcceptedValue = (DomainValue)cbAnswer.SelectedItem,
                        Variable = _workingMemory.CurrentVariable
                    };
                    _workingMemory.CurrentMemory.Add(deducedFact);
                    _isAnswered = true;
                    AddMessage(new Message(Message.MessageType.Answer,
                        "Мой ответ: " + deducedFact.AcceptedValue.Value));
                }
                ));

            }
        }

        private void ProveMainGoal()
        {
            var answer = TryProveGoal(_mainGoal, 0);
            Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() =>
                {
                    if (answer == null)
                    {
                        AddMessage(new Message(Message.MessageType.Question,
                            "К сожалению, искомую цель доказать не удалось. Пожалуйста, обратитесь к другой ЭС.", 
                            "#FFBACC"));
                    }
                    else AddMessage(new Message(Message.MessageType.Question,
                        "Искомая цель консультации доказана! \n" +
                        "Переменная " + answer.Variable.Name + " содержит следующий результат:" +
                        "\n" + answer.AcceptedValue.Value,
                        "#CCFFDC"));
                    cbAnswer.ItemsSource = null;
                    btnSetAnswer.IsEnabled = false;
                    OldExplanation.SetCopy(_mainGoal, _explanationComponent, _workingMemory.CurrentMemory);
                    var explanationWindow = new ExplanationWindow(_explanationComponent, _mainGoal, _workingMemory.CurrentMemory);
                    var label = GenerateExplanationLabel(explanationWindow);
                    AddMessage(new Message(Message.MessageType.Question, label));
                }));
        }

        private Label GenerateExplanationLabel(ExplanationWindow explanationWindow)
        {
            Label mainLabel = new Label();
            TextBlock textBlock = new TextBlock
            {
                TextWrapping = TextWrapping.Wrap
            };
            textBlock.Inlines.Add("Для того, чтобы увидеть объяснение, перейдите по ссылке ниже:\n");
            Hyperlink hyperlink = new Hyperlink();
            hyperlink.Inlines.Add("Показать объяснение.");
            hyperlink.Click += (s, e) => explanationWindow.ShowDialog();
            textBlock.Inlines.Add(hyperlink);
            mainLabel.Content = textBlock;
            return mainLabel;
        }

        public Fact TryProveGoal(Variable variable, int startRule, int level = 0)
        {
            _workingMemory.CurrentVariable = variable;
            var fact = _workingMemory.CurrentMemory.Find(x => x.Variable.Name == variable.Name);
            if (fact != null) return fact;
            if (variable.Type == Variable.VariableType.Requested)
                return GetAnswer(variable);
            var rules = ProductionShell.Data.Rules;
            var range = Enumerable.Range(startRule, rules.Count - startRule).
                Union(Enumerable.Range(0, startRule));
            foreach (var i in range)
            {
                if (_explanationComponent.Rules.Contains(rules[i]))
                    continue;
                bool isContainVariable = rules[i].Conclusions.Any(x => x.Variable.Name == variable.Name);
                // Если нашли  - доказываем левую часть
                if (isContainVariable)
                {
                    bool isSuccessResult = true;
                    foreach (var leftVariable in rules[i].Premises)
                    {
                        var findedFact = TryProveGoal(leftVariable.Variable, 
                            i + 1 < rules.Count ? i + 1 : 0,
                            level++);
                        if (findedFact != null)
                        {
                            if (leftVariable.AcceptedValue.Value != findedFact.AcceptedValue.Value)
                            {
                                isSuccessResult = false;
                                break;
                            }

                        }
                        else isSuccessResult = false;
                    }
                    // Если успешно доказали - правило сработало
                    if (isSuccessResult)
                    {
                        rules[i].Level = level;
                        variable.RuleName = rules[i].Name;
                        _explanationComponent.Rules.Push(rules[i]);
                        _explanationComponent.Goals.Push(variable);
                        Fact resultFact = null;
                        foreach (var rightPart in rules[i].Conclusions)
                        {
                            _workingMemory.CurrentMemory.Add(rightPart);
                            if (rightPart.Variable.Name == variable.Name)
                                resultFact = rightPart;
                        }
                        return resultFact;
                    }
                }
            }
            if (variable.Type == Variable.VariableType.DeducibleRequested)
                return GetAnswer(variable);
            return null;
        }

        private Fact GetAnswer(Variable variable)
        {
            _isAnswered = false;
            Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() =>
                {
                    string question = String.IsNullOrEmpty(variable.Question) ?
                                        $"Значение переменной {variable.Name}?" :
                                        variable.Question;
                    AddMessage(new Message(Message.MessageType.Question, question));
                    cbAnswer.ItemsSource = variable.Domain.Values;
                    cbAnswer.DisplayMemberPath = "Value";
                    cbAnswer.SelectedIndex = 0;
                }));
            while (!_isAnswered) ;

            return _workingMemory.CurrentMemory[_workingMemory.CurrentMemory.Count() - 1];

        }
    }
}
