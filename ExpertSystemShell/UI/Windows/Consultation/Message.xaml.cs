﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpertSystemShell.UI.Windows.Consultation
{
    /// <summary>
    /// Логика взаимодействия для Message.xaml
    /// </summary>
    public partial class Message : UserControl
    {
        private MessageType _msgType;

        public enum MessageType { Answer, Question }

        public MessageType MsgType
        {
            get => _msgType;
            private set
            {
                if (value == MessageType.Answer)
                {
                    brdMessage.Background = ColorFromHex("#D0DFEF");
                    HorizontalAlignment = HorizontalAlignment.Right;
                }
                else
                {
                    brdMessage.Background = ColorFromHex("#FFFFFF");
                    HorizontalAlignment = HorizontalAlignment.Left;
                }
                _msgType = value;                    
            }
        }

        public Message(MessageType msgType, string text)
        {
            InitializeComponent();
            MsgType = msgType;
            tbMessage.Text = text;
        }

        public Message(MessageType msgType, string text, string hexColor)
        {
            InitializeComponent();
            MsgType = msgType;
            tbMessage.Text = text;
            brdMessage.Background = ColorFromHex(hexColor);
        }

        public Message(MessageType msgType, Label label)
        {
            InitializeComponent();
            MsgType = msgType;
            tbMessage.Inlines.Add(label);
        }

        SolidColorBrush ColorFromHex(string hex) =>
                (SolidColorBrush)(new BrushConverter().ConvertFrom(hex));

    }
}
