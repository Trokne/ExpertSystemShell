﻿using ExpertSystemShell.Core;
using ExpertSystemShell.Core.Handlers;
using ExpertSystemShell.Core.KnowledgeStorage;
using ExpertSystemShell.Core.UI.ListViewDropping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell.UI.Windows.Domains
{
    /// <summary>
    /// Логика взаимодействия для DomainListWindow.xaml
    /// </summary>
    public partial class DomainListWindow : Window
    {
        ListViewDragDropManager<Domain> draggable;
        public DomainListWindow()
        {
            InitializeComponent();
            InitializeListView();
            if (lvDomains.Items.Count != 0)
                lvDomains.SelectedIndex = 0;
        }

        private void InitializeListView()
        {
            lvDomains.ItemsSource = ProductionShell.Data.Domains;
            draggable = new ListViewDragDropManager<Domain>(lvDomains);
            ChangeButtonsState(false);
            UpdateLabels();
        }

        private void UpdateLabels()
        {
            gbDomains.Header = $"Домены ({lvDomains.Items.Count})";
            gbCurrentDomain.Header = lvDomains.SelectedIndex == -1 ?
                "Текущий домен" :
                $"Домен {((Domain)lvDomains.SelectedItem).Name.GetShort()}";
        }

        private void btnAddDomain_Click(object sender, RoutedEventArgs e)
        {
            var addWindow = new AddEditDomainWindow();
            addWindow.ShowDialog();
            lvDomains.Items.Refresh();
            lvCurDomain.Items.Refresh();
        }

        private void btnEditDomain_Click(object sender, RoutedEventArgs e)
        {
            var editWindow = new AddEditDomainWindow((Domain)lvDomains.SelectedItem);
            editWindow.ShowDialog();
            lvDomains.Items.Refresh();
            lvCurDomain.Items.Refresh();
        }

        private void btnRemoveDomain_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Удалить выбранный домен? " +
                "Это повлечёт за собой также удаление связанных перееменных и правил.", 
                "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                ProductionShell.Data.RemoveDomain((Domain)lvDomains.SelectedItem);
            }
        }

        private void lvDomains_MouseDown(object sender, MouseButtonEventArgs e)
        {
            HitTestResult r = VisualTreeHelper.HitTest(this, e.GetPosition(this));
            if (r.VisualHit.GetType() != typeof(ListViewItem))
                lvDomains.UnselectAll();
        }

        private void lvDomains_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvDomains.SelectedIndex == -1)
            {
                ChangeButtonsState(false);
                lvCurDomain.ItemsSource = null;
                return;
            }
            lvCurDomain.ItemsSource = ProductionShell.Data.Domains[lvDomains.SelectedIndex].Values;
            ChangeButtonsState(true);
            UpdateLabels();
        }

        private void ChangeButtonsState(bool isEnabled)
         => btnEditDomain.IsEnabled = btnRemoveDomain.IsEnabled = isEnabled;

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control &&
                e.Key == Key.D && lvDomains.SelectedIndex != -1)
            {
                var domain = (Domain)lvDomains.SelectedItem;
                var copyDomain = new Domain(domain);
                copyDomain.Name = StringHelper.GetCopyName(copyDomain.Name, ProductionShell.Data.Domains);
                ProductionShell.Data.Domains.Add(copyDomain);
            }
            else if (e.Key == Key.Delete && lvDomains.SelectedIndex != -1)
                btnRemoveDomain_Click(sender, e);
        }
    }
}
