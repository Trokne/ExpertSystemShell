﻿using ExpertSystemShell.Core;
using ExpertSystemShell.Core.Handlers;
using ExpertSystemShell.Core.KnowledgeStorage;
using ExpertSystemShell.Core.UI.ListViewDropping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell.UI.Windows.Domains
{
    /// <summary>
    /// Логика взаимодействия для AddEditDomainWindow.xaml
    /// </summary>
    public partial class AddEditDomainWindow : Window
    {
        private readonly string _basicName = "Domain";
        private enum WindowType { Create, Edit }
        private WindowType _windowType;
        private Domain _domain;
        private int _sourceIndex;
        private string _sourceName;
        private bool _isClosedByButton;
        private IEnumerable<Variable> _variables;
        private ListViewDragDropManager<DomainValue> draggable;

        public int EditedIndex { get; set; }

        public AddEditDomainWindow()
        {
            InitializeComponent();
            _sourceName = String.Empty;
            InitializeWindow(WindowType.Create);
        }

        public AddEditDomainWindow(Domain domain)
        {
            InitializeComponent();
            _variables = domain.GetVariable();
            _sourceIndex = ProductionShell.Data.Domains.IndexOf(domain);
            _sourceName = domain.Name;
            _domain = new Domain(domain);
            InitializeWindow(WindowType.Edit);
        }

        public string GetDomainName()
        {
            string name;
            do
            {
                name = _basicName + ProductionShell.Data.DomainNameIndex++.ToString();
            }
            while (ProductionShell.Data.Domains.Any(x => x.Name == name));
            return name;
        }

        private void InitializeWindow(WindowType type)
        {
            _isClosedByButton = false;
            _windowType = type;
            EditedIndex = -1;
            switch (_windowType)
            {
                case WindowType.Edit:
                    Title = "Редактирование домена";
                    btnOk.Content = "Сохранить";
                    tbDomainName.Text = _domain.Name;
                    var variable = _domain.GetVariable();
                    break;
                case WindowType.Create:
                    Title = "Создание нового домена";
                    btnOk.Content = "Добавить";
                    tbDomainName.Text = GetDomainName();
                    _domain = new Domain();
                    break;
            }
            tbDomainName.Focus();
            tbDomainName.SelectAll();
            lvValues.ItemsSource = _domain.Values;
            draggable = new ListViewDragDropManager<DomainValue>(lvValues);
            ChangeVariableButtonsState(false);
        }

        private void btnAddVariable_Click(object sender, RoutedEventArgs e)
        {
            var domainValue = tbVariableName.Text.Trim();
            if (!IsCorrectDomainValue(domainValue))
                return;
            _domain.Values.Add(new DomainValue(domainValue));
            tbVariableName.Clear();
        }

        private bool IsCorrectDomainValue(string domainValue, string lastName = "")
        {
            var action = lastName == "" ? "Добавление" : "Изменение";
            if (String.IsNullOrWhiteSpace(domainValue))
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка в наименовании",
                    $"{action} значения невозможно. Не допускается использование пустых значений или одних пробелов.");
                return false;
            }
            if (_domain.Values.Any(x => x.Value.ToUpper() == domainValue.ToUpper() && 
                x.Value.ToUpper() != lastName.ToUpper()))
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка в наименовании",
                    $"{action} значения невозможно. Введенное значение домена уже занято.");
                return false;
            }
            return true;
        }

        private void btnEditValue_Click(object sender, RoutedEventArgs e)
        {
            var domainValue = tbVariableName.Text.Trim();
            if (!IsCorrectDomainValue(domainValue, ((DomainValue)lvValues.SelectedItem).Value))
                return;
            _domain.Values[lvValues.SelectedIndex] = new DomainValue(domainValue);
            tbVariableName.Clear();
        }

        private void lvValues_MouseDown(object sender, MouseButtonEventArgs e)
        {
            HitTestResult r = VisualTreeHelper.HitTest(this, e.GetPosition(this));
            if (r.VisualHit.GetType() != typeof(ListViewItem))
                lvValues.UnselectAll();
        }

        private void lvValues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvValues.SelectedIndex == -1)
            {
                ChangeVariableButtonsState(false);
                gbValues.Header = "ДОБАВЛЕНИЕ значения";
                lblAddNewValue.Visibility = Visibility.Collapsed;
                btnAddValue.Visibility =  Visibility.Visible;
                tbVariableName.Clear();
                return;
            }
            ChangeVariableButtonsState(true);
            gbValues.Header = "РЕДАКТИРОВАНИЕ значения";
            lblAddNewValue.Visibility = Visibility.Visible;
            btnAddValue.Visibility = Visibility.Collapsed;
            tbVariableName.Text = ((DomainValue)lvValues.SelectedItem)?.Value ?? "";
        }

        private void ChangeVariableButtonsState(bool isEnabled)
        {
            //btnEditValue.IsEnabled = isEnabled;
            //btnRemoveValue.IsEnabled = isEnabled;
            btnRemoveValue.Visibility = btnEditValue.Visibility = 
                isEnabled ? Visibility.Visible : Visibility.Collapsed;

        }

        private void btnRemoveValue_Click(object sender, RoutedEventArgs e)
        {
            _domain.Values.RemoveAt(lvValues.SelectedIndex);
            tbVariableName.Clear();
        }

        private void tbVariableName_TextChanged(object sender, TextChangedEventArgs e)
        {
            btnAddValue.IsEnabled = !String.IsNullOrWhiteSpace(tbVariableName.Text);
        }

        private void tbDomainName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbDomainName.Text))
            {
                btnOk.IsEnabled = false;
            }
            else btnOk.IsEnabled = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_isClosedByButton)
            {
                var dialogResult = MessageBox.Show("Сохранить результаты?", 
                    "Внимание!",
                    MessageBoxButton.YesNo, 
                    MessageBoxImage.Question);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    if (!ApplyDomain()) e.Cancel = true;
                }
            }
        }

        private bool ApplyDomain()
        {
            string domainName = tbDomainName.Text.Trim();
            string action = _windowType == WindowType.Create ? "добавить" : "изменить";
            if (_domain.Values.Count == 0)
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка!", $"Невозможно {action} данный домен, список его значений является пустым!");
                return false;
            }
            if (String.IsNullOrWhiteSpace(domainName))
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка!", $"Невозможно {action} данный домен, т.к. его имя является пустым или содержит одни пробелы!");
                return false;
            }
            if (ProductionShell.Data.Domains.Any(x => x.Name.ToUpper() == domainName.ToUpper() && 
                x.Name.ToUpper() != _sourceName.ToUpper()))
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка!", $"Невозможно {action}  данный домен, т.к. домен с данным именем уже существует!");
                return false;
            }
            _domain.Name = domainName;
            var domains = ProductionShell.Data.Domains;
            switch (_windowType)
            {
                case WindowType.Create:
                    domains.Add(_domain);
                    EditedIndex = domains.Count - 1;
                    break;
                case WindowType.Edit:
                    if (_variables == null || _variables.Count() == 0)
                        domains[_sourceIndex] = new Domain(_domain);
                    else
                    {
                        if (Domain.IsEquals(domains[_sourceIndex], _domain))
                        {
                            domains[_sourceIndex].CopyFromDomain(_domain);
                            foreach (var variable in _variables)
                                variable.Domain = domains[_sourceIndex];
                        }
                        else
                        {
                            var dialog = MessageBox.Show("Домен используется в переменных, " +
                                "но множество его значений изменилось. " +
                                "Желаете создать новый домен?",
                                "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                            if (dialog == MessageBoxResult.Yes)
                            {
                                if (ProductionShell.Data.Domains.Any(x => x.Name == _domain.Name))
                                    _domain.Name = StringHelper.GetCopyName(_domain.Name, domains);
                                domains.Add(new Domain(_domain));
                                EditedIndex = domains.Count - 1;
                            }
                            else return false;
                        }
                    }
                    EditedIndex = _sourceIndex;
                    break;
            }
            return true;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            _isClosedByButton = true;
            if (ApplyDomain())
                Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            _isClosedByButton = true;
            Close();
        }

        private void hypAddNewValue_Click(object sender, RoutedEventArgs e)
        {
            lvValues.UnselectAll();
        }
    }
}
