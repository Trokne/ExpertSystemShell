﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ExpertSystemShell.Core;
using ExpertSystemShell.Core.Handlers;
using ExpertSystemShell.Core.KnowledgeStorage;
using ExpertSystemShell.UI.Windows.Domains;

namespace ExpertSystemShell.UI.Windows.Variables
{
    /// <summary>
    /// Логика взаимодействия для AddEditVariableWindow.xaml
    /// </summary>
    public partial class AddEditVariableWindow : Window
    {
        private readonly string _basicName = "Variable";
        private enum WindowType { Create, Edit }
        private WindowType _windowType;
        private Variable _variable;
        private Domain _sourceDomain;
        private string _sourceName;
        private bool _isInRules;
        private int _sourceIndex;
        private bool isClosedByButton;

        public int EditedIndex { get; set; }

        public AddEditVariableWindow()
        {
            InitializeComponent();
            _sourceName = "";
            InitializeWindow(WindowType.Create);
        }

        public AddEditVariableWindow(Variable selectedItem)
        {
            InitializeComponent();
            _sourceName = selectedItem.Name;
            _sourceIndex = ProductionShell.Data.Variables.IndexOf(selectedItem);
            _variable = new Variable(selectedItem);
            var rules = selectedItem.FindInRules();
            _isInRules = rules != null & rules.Count > 0;
            _sourceDomain = _variable.Domain;
            InitializeWindow(WindowType.Edit);
        }


        private void InitializeWindow(WindowType type)
        {
            isClosedByButton = false;
            _windowType = type;
            EditedIndex = -1;
            switch (_windowType)
            {
                case WindowType.Edit:
                    Title = "Редактирование переменной";
                    btnOk.Content = "Сохранить";
                    tbVariableName.Text = _variable.Name;
                    tbQuestion.Text = _variable.Question;
                    break;
                case WindowType.Create:
                    Title = "Создание новой переменной";
                    btnOk.Content = "Добавить";
                    tbVariableName.Text = GetVariableName();
                    _variable = new Variable();
                    break;
            }
            InitializeControls();
        }

        private void InitializeControls()
        {
            tbVariableName.Focus();
            tbVariableName.SelectAll();
            lvDomainValues.ItemsSource = _variable.Domain.Values;
            cbDomain.ItemsSource = ProductionShell.Data.Domains;
            if (ProductionShell.Data.Domains.Count != 0)
            {
                if (_variable.Domain.Values.Count == 0)
                    cbDomain.SelectedIndex = 0;
                else
                {
                    var index = -1;
                    for (int i = 0; i < ProductionShell.Data.Domains.Count; i++)
                    {
                        if (ProductionShell.Data.Domains[i].Name == _variable.Domain.Name)
                        {
                            index = i;
                            break;
                        }
                    }
                    if (index != -1) cbDomain.SelectedIndex = index;
                }
            }
            else btnEditDomain.IsEnabled = false;
            switch (_variable.Type)
            {
                case Variable.VariableType.Deducible:
                    rtbDeducible.IsChecked = true;
                    break;
                case Variable.VariableType.DeducibleRequested:
                    rtbDeducibleRequested.IsChecked = true;
                    break;
                case Variable.VariableType.Requested:
                    rtbRequested.IsChecked = true;
                    break;
            }
        }

        public string GetVariableName()
        {
            string name;
            do
            {
                name = _basicName + ProductionShell.Data.VariableNameIndex++.ToString();
            }
            while (ProductionShell.Data.Domains.Any(x => x.Name == name));
            return name;
        }


        private void btnAddDomain_Click(object sender, RoutedEventArgs e)
        {
            var addDomainWindow = new AddEditDomainWindow();
            addDomainWindow.ShowDialog();
            var resultIndex = addDomainWindow.EditedIndex;
            if (resultIndex != -1)
                cbDomain.SelectedItem = ProductionShell.Data.Domains[resultIndex];
        }

        private void btnEditDomain_Click(object sender, RoutedEventArgs e)
        {
            int selectedIndex = cbDomain.SelectedIndex;
            if (selectedIndex == -1)
                return;
            var editDomainWindow = new AddEditDomainWindow((Domain)cbDomain.SelectedItem);
            editDomainWindow.ShowDialog();
            cbDomain.Items.Refresh();
            cbDomain.SelectedIndex = -1;
            cbDomain.SelectedIndex = selectedIndex;
            lvDomainValues.Items.Refresh();
        }

        private void cbDomain_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbDomain.SelectedIndex == -1)
            {
                btnEditDomain.IsEnabled = false;
                return;
            }
            btnEditDomain.IsEnabled = true;
            _variable.Domain = (Domain)cbDomain.SelectedItem;
            lvDomainValues.ItemsSource = _variable.Domain.Values;
            lvDomainValues.Items.Refresh();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            var rtb = (RadioButton)sender;
            if (rtb.Content == null)
                return;
            if (rtb.Content.ToString() == Variable.VariableType.Deducible.ToDescription())
            {
                tbQuestion.IsEnabled = false;
                tbQuestion.Clear();
                _variable.Type = Variable.VariableType.Deducible;
                _variable.Question = String.Empty;
                return;
            }
            tbQuestion.IsEnabled = true;
            if (rtb.Content.ToString() == Variable.VariableType.DeducibleRequested.ToDescription())
                _variable.Type = Variable.VariableType.DeducibleRequested;
            else if (rtb.Content.ToString() == Variable.VariableType.Requested.ToDescription())
                _variable.Type = Variable.VariableType.Requested;
        }

        private void tbQuestion_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ChangeTextBoxColor();

        }

        private void ChangeTextBoxColor()
        {
            SolidColorBrush GetFromHex(string hex) =>
                (SolidColorBrush)(new BrushConverter().ConvertFrom(hex));
            tbQuestion.Background = tbQuestion.IsEnabled ? GetFromHex("#ffffff") : GetFromHex("#F7F7F7");
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            isClosedByButton = true;
            if (ApplyVariable())
                Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            isClosedByButton = true;
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!isClosedByButton)
            {
                var dialogResult = MessageBox.Show("Сохранить результаты?",
                    "Внимание!",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    if (!ApplyVariable())
                        e.Cancel = true;
                }
            }
        }

        private bool ApplyVariable()
        {
            if (!IsCorrectVariable())
                return false;
            _variable.Name = tbVariableName.Text;
            _variable.Question = tbQuestion.Text;
            var variables = ProductionShell.Data.Variables;
            switch (_windowType)
            {
                case WindowType.Create:
                    variables.Add(_variable);
                    EditedIndex = variables.Count - 1;
                    break;
                case WindowType.Edit:
                    if ((_variable.Name != _sourceName || 
                        !Domain.IsEquals(_variable.Domain, _sourceDomain)) && 
                        _isInRules)
                    {
                        var dialog = MessageBox.Show("Изменение невозможно. Данная переменная используется в правилах. " +
                                "Желаете создать новую переменную - копию редактируемой?",
                                "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                        if (dialog == MessageBoxResult.Yes)
                        {
                            if (variables.Any(x => x.Name.ToUpper() == _variable.Name))
                                _variable.Name = StringHelper.GetCopyName(_variable.Name, variables);
                            variables.Add(_variable);
                            EditedIndex = variables.Count - 1;
                            return true;
                        }
                        else return false;
                    }
                    variables[_sourceIndex].CopyFromVariable(_variable);
                    EditedIndex = _sourceIndex;
                    break;
            }
            return true;
        }

        private bool IsCorrectVariable()
        {
            string action = _windowType == WindowType.Create ? "добавить" : "изменить";
            if (_variable.Domain.Values.Count == 0)
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка!", $"Невозможно {action} данную переменную, т.к. список значений её домена является пустым!");
                return false;
            }
            var name = tbVariableName.Text.Trim();
            if (String.IsNullOrWhiteSpace(name))
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка!", $"Невозможно {action} данную переменную, т.к. её имя является пустым или содержит одни пробелы!");
                return false;
            }
            if (ProductionShell.Data.Variables.Any(x => x.Name.ToUpper() == name.ToUpper() && 
                x.Name.ToUpper() != _sourceName.ToUpper()))
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка!", $"Невозможно {action} данную переменную, т.к. переменная с данным именем уже существует!");
                return false;
            }
            return true;
        }
    }
}
