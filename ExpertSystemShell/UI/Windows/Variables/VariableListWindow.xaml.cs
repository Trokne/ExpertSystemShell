﻿using ExpertSystemShell.Core;
using ExpertSystemShell.Core.Handlers;
using ExpertSystemShell.Core.KnowledgeStorage;
using ExpertSystemShell.Core.UI.ListViewDropping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell.UI.Windows.Variables
{
    /// <summary>
    /// Логика взаимодействия для VariableListWindow.xaml
    /// </summary>
    public partial class VariableListWindow : Window
    {
        private ListViewDragDropManager<Variable> draggableList;

        public VariableListWindow()
        {
            InitializeComponent();
            InitializeListView();
            if (lvVariables.Items.Count != 0)
                lvVariables.SelectedIndex = 0;
        }

        private void InitializeListView()
        {
            lvVariables.ItemsSource = ProductionShell.Data.Variables;
            draggableList = new ListViewDragDropManager<Variable>(lvVariables);
            ChangeButtonsState(false);
            UpdateLabels();
        }

        private void UpdateLabels()
        {
            gbVariables.Header = $"Переменные ({lvVariables.Items.Count})";
            gbCurrentVariable.Header = lvVariables.SelectedIndex == -1 ?
                "Текущая переменная" :
                $"Переменная {((Variable)lvVariables.SelectedItem).Name.GetShort()}";
        }

        private void ChangeButtonsState(bool isEnabledButtons)
            => btnRemoveVariable.IsEnabled = btnEditVariable.IsEnabled = isEnabledButtons;

        private void btnCreateVariable_Click(object sender, RoutedEventArgs e)
        {
            var addWindow = new AddEditVariableWindow();
            addWindow.ShowDialog();
        }

        private void btnEditVariable_Click(object sender, RoutedEventArgs e)
        {
            var editWindow = new AddEditVariableWindow((Variable)lvVariables.SelectedItem);
            editWindow.ShowDialog();
            lvVariables.Items.Refresh();
            UpdateFields();
            
        }

        private void btnRemoveVariable_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Удалить выбранную переменную? " +
                "Это также повлечёт за собой удаление связанных правил.", 
                "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                ProductionShell.Data.RemoveVariable((Variable)lvVariables.SelectedItem);
            }
        }

        private void lvVariables_MouseDown(object sender, MouseButtonEventArgs e)
        {
            HitTestResult r = VisualTreeHelper.HitTest(this, e.GetPosition(this));
            if (r.VisualHit.GetType() != typeof(ListViewItem))
                lvVariables.UnselectAll();
        }

        private void lvVariables_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateFields();
            UpdateLabels();
        }

        private void UpdateFields()
        {
            if (lvVariables.SelectedIndex == -1)
            {
                ChangeButtonsState(false);
                ClearTextBoxes();
                return;
            }
            var variable = (Variable)lvVariables.SelectedItem;
            tbDomainName.Text = variable.Domain.Name;
            tbVariableType.Text = variable.Type.ToDescription();
            tbQuestion.Visibility = variable.Type == Variable.VariableType.Deducible ? Visibility.Collapsed : Visibility.Visible;
            lblQuestionText.Visibility = variable.Type == Variable.VariableType.Deducible ? Visibility.Collapsed : Visibility.Visible;
            tbQuestion.Text = variable.Question ?? "";
            ChangeButtonsState(true);
        }

        private void ClearTextBoxes()
        {
            tbDomainName.Clear();
            tbQuestion.Clear();
            tbVariableType.Clear();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control &&
             e.Key == Key.D && lvVariables.SelectedIndex != -1)
            {
                var variable = (Variable)lvVariables.SelectedItem;
                var copyVariable = new Variable(variable);
                copyVariable.Name = StringHelper.GetCopyName(copyVariable.Name, ProductionShell.Data.Variables);
                ProductionShell.Data.Variables.Add(copyVariable);
            }
            else if (e.Key == Key.Delete && lvVariables.SelectedIndex != -1)
                btnRemoveVariable_Click(sender, e);
        }
    }
}
