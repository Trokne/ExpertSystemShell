﻿using ExpertSystemShell.Core;
using ExpertSystemShell.Core.Handlers;
using ExpertSystemShell.Core.KnowledgeStorage;
using ExpertSystemShell.Core.UI.ListViewDropping;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell.UI.Windows.Rules
{
    /// <summary>
    /// Логика взаимодействия для AddEditRule.xaml
    /// </summary>
    public partial class AddEditRuleWindow : Window
    {
        private enum WindowType { Create, Edit };
        private WindowType _windowType;
        private string _basicName = "Rule";
        private bool _isClosedByButton;
        private string _sourceName;
        private ListViewDragDropManager<Fact> draggableIfList;
        private ListViewDragDropManager<Fact> draggableThenList;

        public Rule CurrentRule { get; private set; }

        public AddEditRuleWindow()
        {
            InitializeComponent();
            CurrentRule = new Rule();
            _sourceName = "";
            InitializeWindow(WindowType.Create);
        }

        public AddEditRuleWindow(Rule rule)
        {
            InitializeComponent();
            CurrentRule = new Rule(rule);
            _sourceName = CurrentRule.Name;
            InitializeWindow(WindowType.Edit);
        }

        private void InitializeWindow(WindowType windowType)
        {
            _windowType = windowType;
            InitializeListView();
            switch (windowType)
            {
                case WindowType.Create:
                    Title = "Создание нового правила";
                    tbRuleName.Text = GetRuleName();
                    break;
                case WindowType.Edit:
                    Title = "Редактирование правила";
                    tbRuleName.Text = CurrentRule.Name;
                    break;
            }
            if (CurrentRule.Premises.Count == 0)
                ChangeStateIfButtons(false);
            else lvIfFacts.SelectedIndex = 0;
            if (CurrentRule.Conclusions.Count == 0)
                ChangeStateThenButtons(false);
            else lvThenFacts.SelectedIndex = 0;

            tbRuleName.SelectAll();
            tbRuleName.Focus();
        }

        private void InitializeListView()
        {
            lvIfFacts.ItemsSource = CurrentRule.Premises;
            lvThenFacts.ItemsSource = CurrentRule.Conclusions;
            draggableIfList = new ListViewDragDropManager<Fact>(lvIfFacts);
            draggableThenList = new ListViewDragDropManager<Fact>(lvThenFacts);
            lvIfFacts.DragEnter += OnListViewDragEnter;
            lvThenFacts.DragEnter += OnListViewDragEnter;
            lvIfFacts.Drop += OnListViewDrop;
            lvThenFacts.Drop += OnListViewDrop;
        }

        public string GetRuleName()
        {
            string name;
            do
            {
                name = _basicName + ProductionShell.Data.RuleNameIndex++.ToString();
            }
            while (ProductionShell.Data.Rules.Any(x => x.Name == name));
            return name;
        }

        private void btnAddIfFact_Click(object sender, RoutedEventArgs e)
        {
            AddIfFact();
        }

        private void AddIfFact(Fact reOpenedFact = null)
        {
            var addWindow = reOpenedFact == null ? 
                new AddEditFactWindow(Fact.FactType.Premise) : 
                new AddEditFactWindow(reOpenedFact);
            if (addWindow.ShowDialog() == true)
            {
                if (CurrentRule.IsContain(addWindow.Fact))
                {
                    MessageBox.Show("Данный фанный факт уже существует в посылках или заключениях. Окно создания факта будет открыто по новой", 
                        "Ошибка создания факта", 
                        MessageBoxButton.OK, 
                        MessageBoxImage.Error);
                    AddIfFact(addWindow.Fact);
                }
                else
                {
                    CurrentRule.Premises.Add(addWindow.Fact);
                    lvIfFacts.Items.Refresh();
                }
            }
        }

        private void btnEditIfFact_Click(object sender, RoutedEventArgs e)
        {
            EditIfFact((Fact)lvIfFacts.SelectedItem, lvIfFacts.SelectedIndex);
        }

        private void EditIfFact(Fact fact, int index)
        {
            var editWindow = new AddEditFactWindow(fact);
            if (editWindow.ShowDialog() == true)
            {
                if (CurrentRule.IsContain(editWindow.Fact) &&
                    !editWindow.Fact.IsEqual(fact))
                {
                    MessageBox.Show("Данный фанный факт уже существует в посылках или заключениях. Окно редактирования факта будет открыто по новой",
                        "Ошибка редактирования факта",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    EditIfFact(editWindow.Fact, index);
                }
                else CurrentRule.Premises[index] = editWindow.Fact;
            }
        }

        private void btnRemoveIfFact_Click(object sender, RoutedEventArgs e)
        {
            CurrentRule.Premises.RemoveAt(lvIfFacts.SelectedIndex);
        }

        private void btnAddThenFact_Click(object sender, RoutedEventArgs e)
        {
            AddThenFact();
        }

        private void AddThenFact(Fact reOpenedFact = null)
        {
            var addWindow = reOpenedFact == null ?
                new AddEditFactWindow(Fact.FactType.Conclusion) :
                new AddEditFactWindow(reOpenedFact);
            if (addWindow.ShowDialog() == true)
            {
                if (CurrentRule.IsContain(addWindow.Fact))
                {
                    MessageBox.Show("Данный фанный факт уже существует в посылках или заключениях. Окно создания факта будет открыто по новой",
                        "Ошибка создания факта",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    AddThenFact(addWindow.Fact);
                }
                else
                {
                    CurrentRule.Conclusions.Add(addWindow.Fact);
                    lvThenFacts.Items.Refresh();
                }
            }
        }

        private void btnEditThenFact_Click(object sender, RoutedEventArgs e)
        {
            EditThenFact((Fact)lvThenFacts.SelectedItem, lvThenFacts.SelectedIndex);
        }

        private void EditThenFact(Fact fact, int index)
        {
            var editWindow = new AddEditFactWindow(fact);
            if (editWindow.ShowDialog() == true)
            {
                if (CurrentRule.IsContain(editWindow.Fact) &&
                    !editWindow.Fact.IsEqual(fact))
                {
                    MessageBox.Show("Данный фанный факт уже существует в посылках или заключениях. Окно редактирования факта будет открыто по новой",
                        "Ошибка редактирования факта",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    EditThenFact(editWindow.Fact, index);
                }
                else CurrentRule.Conclusions[index] = editWindow.Fact;
            }
        }

        private void btnRemoveThenFact_Click(object sender, RoutedEventArgs e)
        {
            CurrentRule.Conclusions.RemoveAt(lvThenFacts.SelectedIndex);
        }

        private void lvIfFacts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChangeStateIfButtons(lvIfFacts.SelectedIndex != -1);
        }

        private void ChangeStateIfButtons(bool isEnabled)
            => btnEditIfFact.IsEnabled = btnRemoveIfFact.IsEnabled = isEnabled;
        private void ChangeStateThenButtons(bool isEnabled)
            => btnEditThenFact.IsEnabled = btnRemoveThenFact.IsEnabled = isEnabled;

        private void lvThenFacts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChangeStateThenButtons(lvThenFacts.SelectedIndex != -1);
        }

        private void lvIfFacts_MouseDown(object sender, MouseButtonEventArgs e)
        {
            HitTestResult r = VisualTreeHelper.HitTest(this, e.GetPosition(this));
            if (r.VisualHit.GetType() != typeof(ListViewItem))
                ((ListView)sender).UnselectAll();
        }

        private bool ApplyRule()
        {
            if (lvIfFacts.Items.Count == 0 ||
                lvThenFacts.Items.Count == 0)
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка в фактах",
                    "Добавление невозможно. Не допускается добавление правила с пустой посылкой или заключением");
                return false;
            }
            var name = tbRuleName.Text.Trim();
            if (String.IsNullOrWhiteSpace(name))
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка в наименовании",
                    "Добавление невозможно. Не допускается использование пробела в правила");
                return false;
            }
            var rules = ProductionShell.Data.Rules;
            if (_windowType == WindowType.Edit 
                && rules.Any(x => x.Name.ToUpper() == name)
                && name != _sourceName)
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка в наименовании",
                    "Добавление невозможно. Введенное значение правила уже занято");
                return false;
            }
            CurrentRule.Name = name;
            return true;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            _isClosedByButton = true;
            if (ApplyRule())
            {
                DialogResult = true;
                Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            _isClosedByButton = true;
            DialogResult = false;
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_isClosedByButton)
            {
                var dialogResult = MessageBox.Show("Сохранить результаты?",
                    "Внимание!",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    if (ApplyRule())
                        DialogResult = true;
                    else e.Cancel = true;
                }
                else DialogResult = false;
            }
        }

        void OnListViewDragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Move;
        }

        void OnListViewDrop(object sender, DragEventArgs e)
        {
            if (e.Effects == DragDropEffects.None)
                return;

            var fact = e.Data.GetData(typeof(Fact)) as Fact;
            if (sender == lvIfFacts)
            {
                if (this.draggableIfList.IsDragInProgress)
                    return;
                // An item was dragged from the bottom ListView into the top ListView
                // so remove that item from the bottom ListView.
                (this.lvThenFacts.ItemsSource as ObservableCollection<Fact>).Remove(fact);
                fact.Type = Fact.FactType.Premise;
            }
            else
            {
                if (this.draggableThenList.IsDragInProgress)
                    return;
                if (fact.Variable.Type == Variable.VariableType.Requested)
                {
                    (this.lvThenFacts.ItemsSource as ObservableCollection<Fact>).Remove(fact);
                    MessageBox.Show("Ошибка! Заключение не может иметь факт с переменной запрашиваемого типа!",
                        "Ошибка перемещения",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                // An item was dragged from the top ListView into the bottom ListView
                // so remove that item from the top ListView.
                (this.lvIfFacts.ItemsSource as ObservableCollection<Fact>).Remove(fact);
                fact.Type = Fact.FactType.Conclusion;
            }
        }

    }
}
