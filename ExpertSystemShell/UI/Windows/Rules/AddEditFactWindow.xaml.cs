﻿using ExpertSystemShell.Core;
using ExpertSystemShell.Core.Handlers;
using ExpertSystemShell.Core.KnowledgeStorage;
using ExpertSystemShell.UI.Windows.Variables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExpertSystemShell.UI.Windows.Rules
{
    /// <summary>
    /// Логика взаимодействия для AddEditFact.xaml
    /// </summary>
    public partial class AddEditFactWindow : Window
    {
        private enum WindowType { Create, Edit }
        private bool _isClosedByButton;

        public Fact Fact { get; private set; }

        public AddEditFactWindow(Fact.FactType factType)
        {
            InitializeComponent();
            Fact = new Fact
            {
                Type = factType
            };
            InitializeWindow(WindowType.Create);
        }

        public AddEditFactWindow(Fact fact)
        {
            InitializeComponent();
            Fact = new Fact(fact);
            InitializeWindow(WindowType.Edit);
        }

        private void InitializeWindow(WindowType windowType)
        {
            _isClosedByButton = false;
            UpdateComboBox();
            if (Fact.Variable.Name != null)
                SelectLeftPartIndex();
            if (Fact.Type == Fact.FactType.Premise)
            {
                gbMain.Header = "Задание условия";
                lblOperator.Content = "==";
            }
            else
            {
                gbMain.Header = "Задание заключения";
                lblOperator.Content = ":=";
            }
            switch (windowType)
            {
                case WindowType.Edit:
                    Title = "Редактирование факта";
                    btnOk.Content = "Сохранить";
                    break;
                case WindowType.Create:
                    Title = "Создание нового факта";
                    btnOk.Content = "Добавить";
                    btnOk.IsEnabled = false;
                    if (cbLeftPart.Items.Count > 0)
                    {
                        cbLeftPart.SelectedIndex = 0;
                        cbRightPart.SelectedIndex = 0;
                    }
                    break;
            }
        }

        private void UpdateComboBox()
        {
            switch (Fact.Type)
            {
                case Fact.FactType.Premise:
                    cbLeftPart.ItemsSource = ProductionShell.Data.Variables;
                    break;
                case Fact.FactType.Conclusion:
                    cbLeftPart.ItemsSource = ProductionShell.Data.Variables
                        .Where(x => x.Type != Variable.VariableType.Requested);
                    break;
            }
            cbLeftPart.Items.Refresh();
        }

        private void SelectLeftPartIndex()
        {
            int index = -1;
            var variables = ((IEnumerable<Variable>)cbLeftPart.ItemsSource).ToList();
            for (int i = 0; i < variables.Count; i++)
            {
                if (variables[i].Name == Fact.Variable.Name)
                {
                    index = i;
                    break;
                }
            }
            cbLeftPart.SelectedIndex = index;
            if (index != -1)
                SelectRightPartIndex(index);
        }

        private void SelectRightPartIndex(int varIndex)
        {
            var rightIndex = 0;
            var variables = ProductionShell.Data.Variables;
            for (int i = 0; i < variables[varIndex].Domain.Values.Count; i++)
            {
                if (Fact.AcceptedValue.Value == variables[varIndex].Domain.Values[i].Value)
                {
                    rightIndex = i;
                    break;
                }
            }
            cbRightPart.SelectedIndex = rightIndex;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            _isClosedByButton = true;
            if (ApplyFact())
            {
                DialogResult = true;
                Close();
            }
        }

        private bool ApplyFact()
        {
            if (cbLeftPart.SelectedIndex == -1 || cbRightPart.SelectedIndex == -1)
            {
                MessageBoxHelper.ShowErrorMessage("Ошибка создания факта", "Невозможно данный факт: необходимо заполнить и посылку, и заключение!");
                return false;
            }
            Fact.Variable = (Variable)cbLeftPart.SelectedItem;
            Fact.AcceptedValue = (DomainValue)cbRightPart.SelectedItem;
            return true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            _isClosedByButton = true;
            DialogResult = false;
            Close();
        }

        private void cbLeftPart_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsCorrectItemSource() || cbLeftPart.SelectedItem == null)
                return;
            cbRightPart.ItemsSource = ((Variable)cbLeftPart.SelectedItem).Domain.Values;
            cbRightPart.SelectedIndex = 0;
        }

        private bool IsCorrectItemSource()
        {
            if (cbLeftPart.SelectedIndex != -1 && cbRightPart.SelectedIndex != -1)
                btnOk.IsEnabled = true;
            else btnOk.IsEnabled = false;
            if (cbLeftPart.SelectedIndex == -1)
            {
                cbRightPart.ItemsSource = null;
                Fact.Variable = null;
                return cbLeftPart.Items.Count != 0;
            }
            if (cbLeftPart.SelectedIndex != -1)
            {
                btnOk.IsEnabled = true;
                return true;
            }
            btnOk.IsEnabled = false;
            Fact.AcceptedValue = null;
            Fact.Variable = null;
            return false;
        }

        private void cbRightPart_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsCorrectItemSource())
                return;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_isClosedByButton)
            {
                var dialogResult = MessageBox.Show("Сохранить результаты?",
                    "Внимание!",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    if (ApplyFact())
                        DialogResult = true;
                    else e.Cancel = true;
                }
                else DialogResult = false;
            }
        }

        private void addVariable_Click(object sender, RoutedEventArgs e)
        {
            var addVariable = new AddEditVariableWindow();
            addVariable.ShowDialog();
            if (addVariable.EditedIndex != -1)
            {
                Variable variable = null;
                if (addVariable.EditedIndex < ProductionShell.Data.Variables.Count)
                    variable = ProductionShell.Data.Variables[addVariable.EditedIndex];
                if (variable == null ||
                    (variable.Type == Variable.VariableType.Requested &&
                    Fact.Type == Fact.FactType.Conclusion))
                    return;
                UpdateComboBox();
                var variableList = ((IEnumerable<Variable>)cbLeftPart.ItemsSource).ToList();
                cbLeftPart.SelectedIndex = cbRightPart.SelectedIndex = -1;
                for (int i = 0; i < variableList.Count; i++)
                {
                    if (variableList[i].Name == variable.Name)
                    {
                        cbLeftPart.SelectedIndex = i;
                        cbRightPart.SelectedIndex = 0;
                        break;
                    }
                }
            }
        }
    }
}
