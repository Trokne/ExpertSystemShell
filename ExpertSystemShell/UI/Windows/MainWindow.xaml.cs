﻿using ExpertSystemShell.Core;
using ExpertSystemShell.Core.Consultation;
using ExpertSystemShell.Core.FileManager;
using ExpertSystemShell.Core.Handlers;
using ExpertSystemShell.Core.KnowledgeStorage;
using ExpertSystemShell.Core.UI.ListViewDropping;
using ExpertSystemShell.UI.Windows.Consultation;
using ExpertSystemShell.UI.Windows.Domains;
using ExpertSystemShell.UI.Windows.Rules;
using ExpertSystemShell.UI.Windows.Variables;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ExpertSystemShell
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ListViewDragDropManager<Rule> draggableRules;

        public MainWindow()
        {
            InitializeComponent();
            InitializeListView();
            ChangeButtonStates();
            Title = FileHandler.GetTitle();
        }

        private void InitializeListView()
        {
            lvRules.ItemsSource = ProductionShell.Data.Rules;
            if (draggableRules == null)
            {
                draggableRules = new ListViewDragDropManager<Rule>(lvRules);
            }
            else draggableRules.ListView = lvRules;
            lvRules.Items.Refresh();
            UpdateLabels();
        }


        private void UpdateLabels()
        {
            gbRules.Header = $"Правила ({lvRules.Items.Count})";
            gbCurrentRule.Header = lvRules.SelectedIndex == -1 ?
                "Текущее правило" :
                $"Правило {((Rule)lvRules.SelectedItem).Name.GetShort()}";
        }

        private void ChangeButtonStates()
        {
            btnEditRule.IsEnabled = btnRemoveRule.IsEnabled = lvRules.SelectedIndex != -1;
        }

        private void mItGetDomainList_Click(object sender, RoutedEventArgs e)
        {
            var domainWindow = new DomainListWindow();
            domainWindow.ShowDialog();
            InitializeListView();
        }

        private void mItGetVariableList_Click(object sender, RoutedEventArgs e)
        {
            var variableList = new VariableListWindow();
            variableList.ShowDialog();
            InitializeListView();
        }

        private void mItExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        private void btnAddRule_Click(object sender, RoutedEventArgs e)
        {
            var addWindow = new AddEditRuleWindow();
            if (addWindow.ShowDialog() == true)
                ProductionShell.Data.Rules.Add(addWindow.CurrentRule);
        }

        private void btnEditRule_Click(object sender, RoutedEventArgs e)
        {
            var editWindow = new AddEditRuleWindow((Rule)lvRules.SelectedItem);
            if (editWindow.ShowDialog() == true)
                ProductionShell.Data.Rules[lvRules.SelectedIndex] = editWindow.CurrentRule;
        }

        private void btnRemoveRule_Click(object sender, RoutedEventArgs e)
        {
            ProductionShell.Data.Rules.RemoveAt(lvRules.SelectedIndex);
        }

        private void lvRules_MouseDown(object sender, MouseButtonEventArgs e)
        {
            HitTestResult r = VisualTreeHelper.HitTest(this, e.GetPosition(this));
            if (r.VisualHit.GetType() != typeof(ListViewItem))
                ((ListView)sender).UnselectAll();
        }

        private void lvRules_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChangeButtonStates();
            UpdateLabels();
            if (lvRules.SelectedIndex != -1)
            {
                lvIfFacts.ItemsSource = ((Rule)lvRules.SelectedItem).Premises;
                lvThenFacts.ItemsSource = ((Rule)lvRules.SelectedItem).Conclusions;
            }
            else
            {
                lvIfFacts.ItemsSource = null;
                lvThenFacts.ItemsSource = null;
            }
        }

        private void mItSaveAs_Click(object sender, RoutedEventArgs e)
        {
            FileHandler.SaveAs();
            Title = FileHandler.GetTitle();
        }

        private void mItOpenConsultation_Click(object sender, RoutedEventArgs e)
        {
            var consultWindow = new ConsultationWindow();
            consultWindow.ShowDialog();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control &&
                e.Key == Key.D && lvRules.SelectedIndex != -1)
            {
                var rule = (Rule)lvRules.SelectedItem;
                var copyRule = new Rule(rule);
                copyRule.Name = StringHelper.GetCopyName(copyRule.Name, ProductionShell.Data.Rules);
                ProductionShell.Data.Rules.Add(copyRule);
            }
            else if (e.Key == Key.Delete && lvRules.SelectedIndex != -1)
                ProductionShell.Data.Rules.RemoveAt(lvRules.SelectedIndex);
            else if (e.Key == Key.F5)
                mItOpenConsultation_Click(sender, e);
        }

        private void cmdNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FileHandler.CheckSave();
            ProductionShell.New();
            Title = FileHandler.GetTitle();
            InitializeListView();
            OldExplanation.Remove();
        }

        private void cmdOpen_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FileHandler.CheckSave();
            FileHandler.Open();
            Title = FileHandler.GetTitle();
            InitializeListView();
        }

        private void cmdSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FileHandler.Save();
            Title = FileHandler.GetTitle();
        }

        private void mItOpenExplanation_Click(object sender, RoutedEventArgs e)
        {
            if (OldExplanation.Explanation == null && 
                OldExplanation.MainGoal == null)
            {
                MessageBox.Show("Пройдите по крайней мере одну консультацию, чтобы увидеть объяснение",
                    "Ошибка открытия консультации", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            var window = new ExplanationWindow(OldExplanation.Explanation, 
                OldExplanation.MainGoal,
                OldExplanation.WorkingMemory);
            window.ShowDialog();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }
    }
}
